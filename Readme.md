# Microsoft Graph API proof-of-concept

A simple(-ish) example of interacting with the Microsoft Graph API.

Currently, it simply logs you into Azure AD and performs some simple API calls access your data.
This is an example of the [authorization code grant](https://docs.microsoft.com/en-us/azure/active-directory/develop/v1-protocols-oauth-code)
flow, typical of a situation where an individual user is asked to authenticate with Azure Active Directory so that
your app may request resources on their behalf.

For batch/daemon workflows, we would need to explore the [client credentials grant](https://docs.microsoft.com/en-us/azure/active-directory/develop/v1-oauth2-client-creds-grant-flow)
flow, which can operate on multiple users' data and requires admin consent.


## Setup
As an OAuth2 app, we need to do some setup on the Microsoft Azure backend before it'll recognize us as a valid client application.

### Copy configuration file
Copy the `application-SAMPLE.yml` template to `application.yml`. You will then be using this to configure the app.

### Create a new test application on the Azure Portal
To test the API with a freshly created app, you'll need an account on the [Azure portal](https://portal.azure.com/#home).

- Navigate to `Azure Active Directory` and then `App Registrations`.
- Click `New registration` and choose a name.
- For `Supported account types`, choose "any organizational directory and personal Microsoft accounts" (this seems to be
the most permissive for testing).
- The `Redirect URI` chosen will be where the Azure AD service will redirect the user after authenticating,
and will receive the request token. This should correspond the `redirectURI` property set in the project's `application.yml`.
- Click `Register`.

### Configure API Key and client secret
* In the `Overview` section of the app settings, the `Application (client) ID` is displayed. Copy this value and enter it
into the project's `application.yml` as the value for `apiKey`.
* Navigate to `Certificates & secrets` and then click `New client secret`.
* Add any description and expiration you'd like.
* After the secret is added, copy its generated value and enter it in the project's `application.yml` as the value for `apiSecret`.

### Add API permissions
* Click `API permissions` for your app in the Azure portal, then `Add a permission`
* From the `Microsoft Graph` API, choose `Delegated permissions` as your app will be acting on behalf of a logged-in user
* Add `Calendars.Read`, `Mail.Read`, and `Users.Read` since these are the resources your application will be requesting from the API.
  When the user logs in for the first time, they'll be able to review and consent to these permissions.

### Run the app
Now we can see it in action. Start up the app and open [http://localhost:8080](http://localhost:8080) in a web browser.
If this is your first time logging in, you'll be redirected to log into your Microsoft account and approve or deny
the requested permissions the app would like to use on your behalf.

After authorizing this access in the browser, Microsoft redirects the user to the `redirectURI` previously configured.
Our app receives this HTTP request containing the _auth code_ and redeems it for an _access token_ which can then be used
to authenticate subsequent requests for secure resources.

See [Graph Explorer](https://developer.microsoft.com/en-us/graph/graph-explorer) for examples of interacting with the Graph API.
