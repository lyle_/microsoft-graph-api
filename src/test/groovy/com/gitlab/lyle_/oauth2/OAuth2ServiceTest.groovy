package com.gitlab.lyle_.oauth2

import com.github.scribejava.core.model.OAuth2AccessToken
import com.github.scribejava.core.oauth.OAuth20Service
import spock.lang.Specification

class OAuth2ServiceTest extends Specification {
	static String tmpDir = System.getProperty('java.io.tmpdir') + "/microsoft-graph-api/"
	OAuth2Service service

	def setup() {
		service = new OAuth2Service(scribeOAuthService: Mock(OAuth20Service))
		// Point the token cache at a temporary directory
		service.metaClass.getTokenStorePath = { "${tmpDir}/${service.TOKEN_STORE_FILENAME}" }
	}

	def cleanup() {
		// Clean up temporary token store between specs
		new File(service.getTokenStorePath()).delete()
	}

	def cleanupSpec() {
		// Delete the temporary directory when we're all done
		new File(tmpDir).delete()
	}

	def "Token store path points to a temp directory for testing"() {
		expect:
			service.getTokenStorePath().startsWith(System.getProperty('java.io.tmpdir'))
	}

	def "Get nonexistent token store"() {
		expect: "the token store should not initially exist"
			! new File(service.getTokenStorePath()).exists()
		when: "we request a nonexistent store"
			File tokenStore = service.getTokenStore()
		then: "it still doesn't exist"
			!tokenStore.exists()
	}

	def "Load access token from nonexistent disk cache"() {
		expect:
			service.loadToken() == null
	}

	def "Store access token to new disk cache"() {
		when:
			OAuth2AccessToken accessToken = new OAuth2AccessToken('asdfafbsadfasfbag')
		then:
			service.storeToken(accessToken)
		and:
			service.loadToken() == accessToken
	}

	def "Replace access token in an existing disk cache"() {
		given:
			OAuth2AccessToken token1 = new OAuth2AccessToken('asdfafbsadfasfbag')
			OAuth2AccessToken token2 = new OAuth2AccessToken('12434535345653455')
		when:
			service.storeToken(token1)
			service.storeToken(token2)
		then:
			service.loadToken() == token2
	}

	def "Redeem authorization code for access token"() {
		given:
			def accessToken = new OAuth2AccessToken('zzscvzxcv')
		when:
			1 * service.scribeOAuthService.getAccessToken('authCode') >> { accessToken }
			service.redeemAuthorizationCode('authCode')
		then:
			service.loadToken() == accessToken
	}

	def "Get access token when cached in memory"() {
		given: 'an in-memory token'
			OAuth2AccessToken inMemoryToken = new OAuth2AccessToken('token')
			service.accessToken = inMemoryToken
		when:
			def accessToken = service.getAccessToken()
		then:
			accessToken == inMemoryToken
	}

	def "Get access token when cached on disk"() {
		given: 'no in-memory token, but one cached on disk'
			OAuth2AccessToken onDiskToken = new OAuth2AccessToken('token')
			service.accessToken = null // nothing cached in memory
			service.storeToken(onDiskToken)
		when:
			def accessToken = service.getAccessToken()
		then:
			accessToken == onDiskToken
	}

}
