package com.gitlab.lyle_

import com.gitlab.lyle_.oauth2.OAuth2Service
import com.gitlab.lyle_.oauth2.OAuth2Service.AuthRequiredException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

import javax.servlet.http.HttpServletResponse

@RestController
class ApiRestController {
	@Autowired OAuth2Service oAuth2Service
	Logger log = LoggerFactory.getLogger(this.class)

	String meUrl = 'https://graph.microsoft.com/v1.0/me'
	String messagesUrl = 'https://graph.microsoft.com/v1.0/me/messages'
	String eventsUrl = 'https://graph.microsoft.com/v1.0/me/calendarview?startdatetime=2019-05-07T23:28:55.586Z&enddatetime=2019-05-14T23:28:55.586Z'
	String joinedTeamsUrl = 'https://graph.microsoft.com/v1.0/me/joinedTeams'

	/**
	 * Displays information about the logged-in user.
	 */
	@GetMapping('/')
	String me(HttpServletResponse response) {
		try {
			return """
			<html>
				<head><title>Graph API Demo</title></head>
				<body>
					<h1>Graph API Demo</h1>
					<h2>Profile (/me)</h2> <p>${get(meUrl)}</p>\n
					<h2>Teams (/me/joinedTeams)</h2> <p>${get(joinedTeamsUrl)}</p>\n
					<h2>Messages(/me/messages)</h2> <p>${get(messagesUrl)}</p>
					<h2>Events(/me/calendarview)</h2> <p>${get(eventsUrl)}</p>
				</body>
			</html>"""
		} catch (AuthRequiredException e) {
			def authUrl = oAuth2Service.getAuthorizationUrl('/')
			log.info "No valid access token present, redirecting to $authUrl"
			response.sendRedirect(authUrl)
		}
	}

	/**
	 * Handles the redirect from Azure AD's authorization endpoint after the user has
	 * authorized access to their resources.
	 *
	 * @param authCode The authorization code that the application requested. This is
	 *        subsequently used to get an access token.
	 * @param redirectUrl the application URL the user was trying to access before
	 *        being interrupted by authentication
	 * @return redirects the user to the resource they were trying to access
	 */
	@GetMapping('/auth')
	def receiveAuthCode(@RequestParam('code') String authCode,
	                    @RequestParam('state') String redirectUrl,
	                    HttpServletResponse response) {
		// Use the authCode to get an access token
		log.info "Trading authorization code for an access token"
		oAuth2Service.redeemAuthorizationCode(authCode)
		response.sendRedirect(redirectUrl ?: '/')
	}

	private def get(String microsoftAPIUrl) {
		oAuth2Service.get(microsoftAPIUrl)
	}
}
