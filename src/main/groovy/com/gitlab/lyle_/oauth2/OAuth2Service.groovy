package com.gitlab.lyle_.oauth2

import com.github.scribejava.core.model.OAuth2AccessToken
import com.github.scribejava.core.model.OAuthRequest
import com.github.scribejava.core.model.Response
import com.github.scribejava.core.model.Verb
import com.github.scribejava.core.oauth.OAuth20Service
import groovy.json.JsonSlurper
import io.github.soc.directories.ProjectDirectories
import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

import javax.security.auth.message.AuthException

/**
 * Handles interacting with Azure Active Directory's OAuth2 implementation to maintain a valid access token.
 */
@Service
class OAuth2Service {
	@Value('${spring.application.name}')
	String appName
	@Autowired
	OAuth20Service scribeOAuthService

	/** The filename of the serialized token storeToken */
	final String TOKEN_STORE_FILENAME = 'accessToken.ser'
	Logger log = org.slf4j.LoggerFactory.getLogger(this.class)
	JsonSlurper jsonSlurper = new JsonSlurper()
	private OAuth2AccessToken accessToken

	/**
	 * Performs an OAuth2-authenticated GET request on the provided resource
	 * @param microsoftAPIUrl the OAuth2-protected Microsoft API resource to GET
	 * @return the resource result, or status code if non-200
	 * @throws AuthException if no valid access token is present
	 */
	def get(String microsoftAPIUrl) throws AuthException {
		OAuth2AccessToken accessToken = getAccessToken()
		if (accessToken) {
			OAuthRequest request = new OAuthRequest(Verb.GET, microsoftAPIUrl)
			scribeOAuthService.signRequest(accessToken, request)
			log.trace "Requesting $microsoftAPIUrl"
			Response response = scribeOAuthService.execute(request)
			if (response.isSuccessful()) {
				return jsonSlurper.parseText(response.body)
			} else {
				log.info "API returned ${response.code} for $microsoftAPIUrl"
				return "ERROR: ${response.code}"
			}
		} else {
			throw new AuthRequiredException()
		}
	}

	/**
	 * Given an authorization code, asks Azure AD for an access token.
	 * @param authCode The authorization code returned by Azure AD after the user has authorized
	 *        access to their resources. This is redeemed for an access token.
	 */
	void redeemAuthorizationCode(String authCode) {
		accessToken = scribeOAuthService.getAccessToken(authCode)
		storeToken(accessToken)
	}

	/**
	 * Produces a current OAuth2 access token. Handles disk caching, renewal, and user prompting.
 	 * @return an OAuth2AccessToken which can be used to authorize resource requests; can be
	 *         null if we don't have a current token
	 */
	OAuth2AccessToken getAccessToken() {
		// Use the in-memory access token if present, otherwise see if there's one cached on disk
		accessToken ?: loadToken()
	}

	/**
	 * Generates a URL for a user to authenticate and be redirected back to what they were doing.
	 * @param appResourceUrl the application resource the user is trying to access; they will
	 *        be redirected here after authenticating
	 * @return the URL the user needs to access to log in and authorize us to access their resources
	 */
	String getAuthorizationUrl(String appResourceUrl) {
		return scribeOAuthService.getAuthorizationUrl(appResourceUrl)
	}

	// Serializes the given access token to disk
	private void storeToken(OAuth2AccessToken accessToken) {
		File tokenStore = getTokenStore()
		if (tokenStore.exists()) {
			log.debug("Deleting existing token storeToken at {}", tokenStore.path)
			tokenStore.delete()
		}
		if (!tokenStore.parentFile.exists()) {
			log.debug("Creating token cache directory at {}", tokenStore.parentFile.path)
			if (!tokenStore.parentFile.mkdirs()) {
				throw new IOException("Unable to create temp directory ${tokenStore.parentFile.path}")
			}
		}
		ObjectOutputStream oos
		try {
			log.debug("Storing access token to {}", tokenStore.path)
			oos = tokenStore.newObjectOutputStream()
			oos.writeObject(accessToken)
		} finally {
			oos?.close()
		}
	}

	// Loads an OAuth2 access token from the on-disk token store
	private OAuth2AccessToken loadToken() {
		File tokenStore = getTokenStore()
		if (!tokenStore.exists()) {
			log.debug("No token store exists on disk at {}", tokenStore.path)
			return null
		}

		ObjectInputStream ois
		try {
			ois = tokenStore.newObjectInputStream(this.class.classLoader)
			log.debug("Loading token store from {}", tokenStore.path)
			return ois.readObject()
		} finally {
			ois?.close()
		}
	}

	// Returns the on-disk token store
	private File getTokenStore() {
		return new File(getTokenStorePath())
	}

	// Calculates the file path of the serialized token store
	String getTokenStorePath() {
		ProjectDirectories
			.from('', '', 'Microsoft Graph API example')
			.cacheDir + "/${TOKEN_STORE_FILENAME}"
	}

	class AuthRequiredException extends Exception { }
}
