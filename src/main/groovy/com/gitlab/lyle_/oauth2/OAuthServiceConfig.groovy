package com.gitlab.lyle_.oauth2

import com.github.scribejava.core.builder.ServiceBuilder
import com.github.scribejava.core.oauth.OAuth20Service
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

/**
 * Configures the underlying service library we use to interact with Azure AD's OAuth2 implementation.
 */
@Configuration
class OAuthServiceConfig {
	/* The URL that Azure AD will send the request token to on our end */
	@Value('${redirectURI}')
	String redirectURI
	/* The scopes we'll be using for this service */
	@Value('${scope}')
	String scope
	@Value('${apiKey}')
	String apiKey
	@Value('${apiSecret}')
	String apiSecret

	@Bean
	OAuth20Service oAuthService() {
		return new ServiceBuilder(apiKey)
			.apiSecret(apiSecret)
			.defaultScope(scope)
			.callback(redirectURI)
			.build(com.github.scribejava.apis.MicrosoftAzureActiveDirectory20Api.instance())
	}
}
